package com.sofittech.callerannouncement;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.speech.tts.TextToSpeech;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Service{
	

	
	private Speaker speaker;	

	public static String text ;
	public static 	String sender;
	public static String name;
	String incomingNumber;
	private BroadcastReceiver smsReceiver;

	Boolean call,msg;
	public static final String MyPREFERENCES = "Prefs";
	public static String sms= "android.provider.Telephony.SMS_RECEIVED";

	public static boolean ring ,msgalert =false;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Let it continue running until it is stopped.
		Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

		initializeSMSReceiver();
		registerSMSReceiver();

		return START_STICKY;
	}


	private void initializeSMSReceiver(){
		smsReceiver = new BroadcastReceiver(){
			@Override
			public void onReceive(Context context, Intent intent) {

				call = context.getSharedPreferences(MyPREFERENCES, context.MODE_PRIVATE).getBoolean("call", true);
				msg = context.getSharedPreferences(MyPREFERENCES, context.MODE_PRIVATE).getBoolean("text", true);


				if(intent.getAction().equals(sms)) {
					Log.e("message", "recieved");
					if(msg) {
						 msgalert =true;
						speaker = new Speaker(MainActivity.this);

						Bundle bundle = intent.getExtras();
						if (bundle != null) {
							Object[] pdus = (Object[]) bundle.get("pdus");
							for (int i = 0; i < pdus.length; i++) {
								byte[] pdu = (byte[]) pdus[i];
								SmsMessage message = SmsMessage.createFromPdu(pdu);
								text = message.getDisplayMessageBody();
								sender = getContactName(message.getOriginatingAddress());
								Log.e("sender", sender);
								//
							}
						}
					}
				}

				try {

					String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);


					if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {

						Toast.makeText(context, "Phone Is Ringing", Toast.LENGTH_LONG).show();

						if (call) {
							ring = true;
							speaker = new Speaker(MainActivity.this);
							// Your Code
							 incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

							//	msg += ". Incoming number is " + incomingNumber;
							name = getContactName(incomingNumber);
							Log.e("name", name);

						}
					}
				}catch(Exception e)
				{

				}

				}
		};
	}

	private void registerSMSReceiver() {
		IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
		registerReceiver(smsReceiver, intentFilter);
		IntentFilter intentFilter2 = new IntentFilter("android.intent.action.PHONE_STATE");
		registerReceiver(smsReceiver, intentFilter2);
	}

	private String getContactName(String phone){
		Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone));
		String projection[] = new String[]{ContactsContract.Data.DISPLAY_NAME};
		Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
		if(cursor.moveToFirst()){

			return cursor.getString(0);
		}else {
			return "unknown number";
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(smsReceiver);
		speaker.destroy();
	}



}
