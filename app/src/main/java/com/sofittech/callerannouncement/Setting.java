package com.sofittech.callerannouncement;

import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.CompoundButton;

import com.kyleduo.switchbutton.SwitchButton;

public class Setting extends AppCompatActivity {
    public static final String MyPREFERENCES = "Prefs";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);


      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Boolean call = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).getBoolean("call", true);
        Boolean texts = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).getBoolean("text", true);


        SwitchButton toggleBtn = (SwitchButton) findViewById(R.id.toggle);
        SwitchButton text = (SwitchButton) findViewById(R.id.text);



        if(call)
        {
            toggleBtn.setChecked(true);

        }
        else
        {
            toggleBtn.setChecked(false);
        }
        if(texts)
        {
            text.setChecked(true);

        }
        else
        {
            text.setChecked(false);
        }



        toggleBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {


                  			@Override
            			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                     if(isChecked)
                                     {
                                         getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit().putBoolean("call", true).commit();

                                         Log.e("call checked ", "true");
                                         startService(new Intent(getBaseContext(), MainActivity.class));
                                     }
                                else {
                                         getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit().putBoolean("call", false).commit();

                                         Log.e("call checked ", "false");
                                         startService(new Intent(getBaseContext(), MainActivity.class));
                                     }
                 			}
             		});
        text.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {


            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Log.e("text checked ", "true");
                    getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit().putBoolean("text", true).commit();
                    startService(new Intent(getBaseContext(), MainActivity.class));

                }
                else {
                    getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit().putBoolean("text", false).commit();

                    Log.e("text checked ", "false");
                    startService(new Intent(getBaseContext(), MainActivity.class));
                }
            }
        });

        startService(new Intent(getBaseContext(), MainActivity.class));



    }
}
