package com.sofittech.callerannouncement;

import java.util.HashMap;
import java.util.Locale;

import android.content.Context;
import android.media.AudioManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;

public class Speaker implements OnInitListener {

		private TextToSpeech tts;

		private boolean ready = false;

		private boolean allowed = false;

		public Speaker(Context context){
			Log.e("Constructor","Speak");
			tts = new TextToSpeech(context, this);
		}

		public boolean isAllowed(){
			Log.e("speech"," isAllowed)");
			return allowed;
		}

		public void allow(boolean allowed){
			Log.e("allow", String.valueOf(allowed));
			this.allowed = allowed;
		}
	private final int LONG_DURATION = 5000;
	private final int SHORT_DURATION = 1200;
		@Override
		public void onInit(int status) {
		if(status == TextToSpeech.SUCCESS){
			// Change this to match your
			// locale
			Log.e("status", " TextToSpeech.SUCCESS)");
			tts.setLanguage(Locale.US);
           if(MainActivity.ring) {
			   speak(MainActivity.name + "is calling !");
			   MainActivity.ring=false;
		   }
			if(MainActivity.msgalert) {
			pause(LONG_DURATION);
			speak("You have a new message from" + MainActivity.sender + "!");
			pause(SHORT_DURATION);
			speak(MainActivity.text);
				MainActivity.msgalert=false;
			}


			ready = true;
		}else{
			ready = false;
		}
	}
	
	public void speak(String text){

		// Speak only if the TTS is ready
		// and the user has allowed speech
		
	//	if(ready && allowed) {
			Log.e("speak"," speak");
			HashMap<String, String> hash = new HashMap<String,String>();
			hash.put(TextToSpeech.Engine.KEY_PARAM_STREAM, 
					String.valueOf(AudioManager.STREAM_NOTIFICATION));
			tts.speak(text, TextToSpeech.QUEUE_ADD, hash);
	//	}
	}
	
	public void pause(int duration){
		tts.playSilence(duration, TextToSpeech.QUEUE_ADD, null);
	}
		
	// Free up resources
	public void destroy(){
		tts.shutdown();
	}
	
}
