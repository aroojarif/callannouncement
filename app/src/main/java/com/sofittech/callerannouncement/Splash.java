package com.sofittech.callerannouncement;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        handler.postDelayed(r, 3000);
    }


    Handler handler = new Handler();
    Runnable r = new Runnable() {
        @Override
        public void run() {
//

            Intent i = new Intent(Splash.this, Setting.class);
            startActivity(i);
            Splash.this.finish();

        }
    };
}
